#ifndef  _PARTITION_H
#define  _PARTITION_H


#include "sys.h"

#define BOOT_BASE_ADDR                  0x08000000
#define PARAMETERS_TAB_BASE_ADDR        0x0800B800
#define APP_BASE_ADDR                   0x0800C000
#define APP_BAK_BASE_ADDR               0x08044000
#define FIRMWARE_BASE_ADDR              0x0807C000

typedef struct parameters_table
{
	/* 验证分区的是否存在 */
	uint16_t parameters_flag;
	/* 应用程序升级标志 */
	uint16_t app_update_flag;
	/* bootloader的地址 */
	uint32_t boot_addr;
	/* 应用程序的地址 */
	uint32_t app_address;
	/* app 升级备份区域地址 */
	uint32_t app_bak_address;
	/* 固件存放地址 */
	uint32_t firmware_address;
	/* bootloader和app的交换数据 */
	uint32_t data_exchange;
}Parameters_Table_t;



s8 Check_Parament_Tab(void);
s8 Parament_Tab_Init(void);


#endif   //_PARTITION_H


