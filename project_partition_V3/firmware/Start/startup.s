                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     RW_And_ZI_Init              ;初始化RW段和ZI段
	            DCD     Num_Inc                     ;数字自增
                DCD     Num_Dec                     ;数字自减
				DCD     Get_Num                     ;获取数字的值
			    DCD     Get_Num1
				DCD     Get_Num_Addr
				DCD	    Get_Flag
				DCD     Swap_Num                    ;交换数据
                DCD     My_Men_Copy                 ;内存copy					
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY
				EXPORT  RW_And_ZI_Init            [WEAK]
				EXPORT  Num_Inc                   [WEAK]
				EXPORT  Num_Dec                   [WEAK]
				EXPORT  Get_Num                   [WEAK]
				EXPORT  Get_Num1                  [WEAK]
				EXPORT  Get_Num_Addr              [WEAK]
				EXPORT	Get_Flag                  [WEAK]
				EXPORT  Swap_Num                  [WEAK]
				EXPORT  My_Men_Copy               [WEAK]
			    
RW_And_ZI_Init  	
Num_Inc
Num_Dec
Get_Num
Get_Num1
Get_Num_Addr
Get_Flag
Swap_Num
My_Men_Copy

                B   .
                END