#ifndef _SYS_USART_H
#define _SYS_USART_H

#include "sys.h"


#define	USART1_TX_GPIO				GPIOA
#define	USART1_TX_PIN				GPIO_Pin_9
#define	USART1_TX_CLK				RCC_APB2Periph_GPIOA
#define USART1_TX_SOURCE            GPIO_PinSource9

#define	USART1_RX_GPIO				GPIOA
#define	USART1_RX_PIN				GPIO_Pin_10
#define	USART1_RX_CLK				RCC_APB2Periph_GPIOA
#define USART1_RX_SOURCE            GPIO_PinSource10

#define USART1_CLK                  RCC_APB2Periph_USART1

#define	USART1_DMA_TX_CLK			RCC_AHBPeriph_DMA1
#define	USART1_DMA_RX_CLK			RCC_AHBPeriph_DMA1
#define	USART1_DMA_TX_CHANNEL		DMA1_Channel4
#define	USART1_DMA_RX_CHANNEL		DMA1_Channel5
#define	USART1_DMA_TX_FLAG			DMA1_FLAG_TC4
#define	USART1_DMA_RX_FLAG			DMA1_FLAG_TC5
#define	USART1_DMA_TX_IT			DMA1_IT_TC4
#define	USART1_DMA_RX_IT			DMA1_IT_TC5
#define	USART1_DMA_TX_IRQn			DMA1_Channel4_IRQn
#define	USART1_DMA_RX_IRQn			DMA1_Channel5_IRQn
#define	USART1_DMA_TX_IRQHandler	DMA1_Channel4_IRQHandler
#define	USART1_DMA_RX_IRQHandler	DMA1_Channel5_IRQHandler


#define SYS_TX_BUFF_LEN (128)



void USART1_GPIO_Config(void);
void USART1_Config(u32 baudrate);
void USART1_DMA_TX_Config(u8* Buffer, s32 NumData);
void USART1_DMA_RX_Config(u8* Buffer, s32 NumData);
void USART1_Init(u32 baudrate);
void USART1_Enable(void);
void USART1_Disable(void);
void USART1_Printf(const char *format, ...);


#endif

