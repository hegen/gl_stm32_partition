#ifndef _SYSTICK_H
#define _SYSTICK_H

#include "sys.h"

void SYSTICK_Init(u32 period);
u32 SYSTICK_GetTime(void);
void SYSTICK_Delay_us(volatile u32 tim);
void SYSTICK_Delay_ms(volatile u32 tim);


#endif //_SYSTICK_H


